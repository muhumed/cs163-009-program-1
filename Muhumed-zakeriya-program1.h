//Zakeriya Muhumed || CS163 || Assignemnt 1
//Library 
using namespace std; 
#include <iostream>
#include <iomanip>
#include <cctype>
#include <cstring>
#include <string>

//shows infomation
struct show{
    char * name;
    char * genre;
    int seasons;
    int eps;
    float rating;
    show * showNext;
};

//sources
struct node {
    char * source;
    node * next;
    show * showHead;
};

//class
class flims{ 
    public:
        flims(); //Task #1
        ~flims();// Task #7
        int add_source(const char * input_sources); //Task #2
        int add_show(show & new_show,const char * input_sources); //Task #4
        int remove(const char * input_sources); //Task #6
        int display_sources(); //Task #3
        int display_list(const char * input_sources); //Task #5 
        int display_genre(const char * genres); //Task #8

    private:
        node * head;

};
