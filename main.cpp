/******************************************************************************
# Author:           Zakeriya Muhumed
# Assignment:       A01 (CS163)
# Date:             Jan 16, 2022
# Sources:          Assignment 1 instruction
#******************************************************************************/
#include "Muhumed-zakeriya-program1.h"
int main(){ 
    flims user_class;
    show new_show;
    int choice;
    char * input_sources;
    char * genres;

    while(choice != 7){
        cout<< "Pick 1-7 options" << endl;
        cout <<"\t1) Add a new sources"<< endl;
        cout <<"\t2) Add a new show" << endl;
        cout <<"\t3) Remove a sources" << endl;
        cout <<"\t4) Display all sources" << endl;//Task #3
        cout <<"\t5) Display show recommend by a source" <<endl;
        cout <<"\t6) Display a specific genre" << endl;
        cout <<"\t7) Quit" << endl;
        cout <<"\n\t\t--->";
        cin >> choice; 
        cin.ignore();	
        while((choice < 1 && choice > 7) || (!cin)){
            cin>> choice;
            cin.ignore();
        }
        switch(choice){
            case 1://Adds a source
                input_sources= new char[80];

                cout <<" Enter the sources name add: ";
                cin.get(input_sources,80, '\n');
                cin.clear();
                cin.ignore(100, '\n');

                user_class.add_source(input_sources);

                delete [] input_sources;
                input_sources=nullptr;

                break;
            case 2://Add a new show
                input_sources= new char[100];

                cout <<"\t\nWho recommended this show? ";
                cin.get(input_sources,100, '\n');
                cin.clear();
                cin.ignore(100, '\n');

                new_show.name= new char[100];
                cout <<"\t\nTitle: ";
                cin.get(new_show.name, '\n');
                cin.clear();
                cin.ignore(100, '\n');

                new_show.genre= new char[100];
                cout <<"\t\nGenre: ";
                cin.get(new_show.genre, '\n');
                cin.clear();
                cin.ignore(100, '\n');

                cout << "\t\nNumber of seasons:";
                cin >> new_show.seasons;
                cin.clear();
                cin.ignore(100, '\n');

                cout<< "\t\nNumber of episodes:";
                cin >> new_show.eps;
                cin.clear();
                cin.ignore(100, '\n');

                cout <<"\t\nRating: ";
                cin >> new_show.rating;
                cin.clear();
                cin.ignore(100, '\n');

                user_class.add_show(new_show, input_sources);

                delete [] input_sources;
                delete [] new_show.name;
                delete [] new_show.genre;

                input_sources=nullptr;
                new_show.name=nullptr;
                new_show.genre=nullptr;
                break;
            case 3://Remove a sources
                input_sources= new char[100];
                cout <<"\nEnter the sources name remove: ";
                cin.get(input_sources,100,'\n');
                cin.clear();
                cin.ignore(100, '\n');

                user_class.remove(input_sources);

                delete [] input_sources;
                input_sources=nullptr;
                break;
            case 4://Display all sources		
                user_class.display_sources(); //Task #3
                break;	
            case 5:// Display show recommend by a source 
                input_sources= new char[100];
                cout <<" Enter the sources to display: ";
                cin.get(input_sources,100, '\n');
                cin.clear();
                cin.ignore(100, '\n');

                user_class.display_list(input_sources);

                delete [] input_sources;
                input_sources=nullptr;
                break;
            case 6://Display a specific genre
                genres= new char[100];
                cout <<" Enter the genre to display: ";
                cin.get(genres,100, '\n');
                cin.clear();
                cin.ignore(100, '\n');
                user_class.display_genre(genres);
                delete [] genres;
                genres=nullptr;
                break;
            case 7://QUIT
                cout << "Thank you for using this program" << endl;
                break;

        }
    }
    return 0; 
}
