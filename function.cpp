//Zakeriya Muhumed || CS163 || Assignemnt 1
#include "Muhumed-zakeriya-program1.h"
//Constructor
flims::flims(){ //Task #1
    head = nullptr;
}
//Destuctor 
flims::~flims(){ //Task #7
    node * temp;
    while(head){
        temp = head-> next;
        delete head;
        head = temp;
    }
}
int flims::add_source(const char * add_source){ //Task #2 
    if(!head){ 
        head = new node;
        head->source = new char[strlen(add_source)+1];
        strcpy(head->source, add_source);
        head->next= NULL;
        head->showHead= NULL;

        return 1;
    }
    else{
        node * current_node = head;
        while(current_node->next){
            if(strcmp(current_node->source, add_source) == 0){
                return 0;
            }
            current_node= current_node->next;
        }
        current_node->next = new node;
        current_node->next->source = new char[strlen(add_source)+1];
        strcpy(current_node->next->source, add_source);
        current_node->next->next= NULL;
        current_node->next->showHead= NULL;

    }

    return 1;
}
int flims::add_show(show & new_show, const char * add_source){ //Task #4
    if(!add_source){
        return 0;
    }
    node * current_node = head;
    while(current_node){
        if(strcmp(current_node->source, add_source) ==0){
            if(!current_node->showHead){
                current_node->showHead = new show;
                current_node->showHead->name = new char[strlen(new_show.name)+1];
                strcpy(current_node->showHead->name, new_show.name);
                current_node->showHead->genre = new char[strlen(new_show.genre)+1];
                strcpy(current_node->showHead->genre, new_show.genre);
                current_node->showHead->seasons = new_show.seasons;
                current_node->showHead->eps = new_show.eps;
                current_node->showHead->rating = new_show.rating;
                current_node->showHead->showNext = NULL;

                return 1;
            }
            else{
                show * current_show = current_node->showHead;
                while(current_show->showNext){
                    current_show = current_show->showNext;
                }
                current_show->showNext = new show;
                current_show->showNext->name = new char[strlen(new_show.name)+1];
                strcpy(current_show->showNext->name, new_show.name);
                current_show->showNext->genre = new char[strlen(new_show.genre)+1];
                strcpy(current_show->showNext->genre, new_show.genre);
                current_show->showNext->seasons = new_show.seasons;
                current_show->showNext->eps = new_show.eps;
                current_show->showNext->rating = new_show.rating;
                current_show->showNext->showNext = NULL;

                return 1;
            }

        }
        current_node = current_node->next;
    }
    return 0;
}
int flims::remove(const char * delete_sources){ //Task #6

    node * current_node = head;

    if(!delete_sources || !head){
        return 0;
    }
    if((!head->next)|| (strcmp(head->source, delete_sources)==0)){
        head=head->next;
        delete current_node;
        return 1;
    }
    else{
        while(current_node->next){
            if(strcmp(current_node->next->source, delete_sources)==0){
                node * temp = current_node->next-> next;

                delete current_node->next;
                current_node->next = temp;
                return 1;
            }
            current_node = current_node->next;
        }
        return 0;
    }
}
int flims::display_sources(){ //Task #3

    int i =1;
    node * current_node = head;
    if(!head){
        return 0;
    }
    cout << "Sources: " << endl;

    while(current_node){
        cout << i << ". " << current_node->source << endl;
        i++;
        current_node = current_node->next;
    }

    return 1;
}
int flims::display_list(const char * input_sources){ //Task #5

    int i =1;
    node * current_node = head;
    if(!head){
        return 0;
    }
    while(current_node){
        if(strcmp(current_node->source, input_sources) ==0){
            show * current_show= current_node->showHead;
            while(current_show){
                cout << i << ". " << input_sources;
                cout << "\tName: " << current_show->name << endl;
                cout << "\tGenre: " << current_show->genre << endl;
                cout << "\tSeasons: " << current_show->seasons << endl;
                cout << "\tEpisodes: " << current_show->eps << endl;
                cout << "\tRating: " << current_show->rating << endl;
                i++;
                current_show= current_show->showNext;
            }
        }
        current_node= current_node->next; 
    }

    return 0;
}

int flims::display_genre(const char * genres){//Task #8
    int i =1;
    node *current_node = head; 
    if(!head){
        return 0;
    }
    while(current_node){
        show * temp_show= current_node->showHead;
        while(temp_show){
            if(strcmp(temp_show->genre, genres) ==0){
                if(i == 1){
                    cout << "Genre " << genres << " found";
                }
                cout << endl << i << ". " << genres << endl;
                cout << "\tName: " << temp_show->name << endl;
                cout << "\tGenre: " << temp_show->genre << endl;
                cout << "\tSeasons: " << temp_show->seasons << endl;
                cout << "\tEpisodes: " << temp_show->eps << endl;
                cout << "\tRating: " << temp_show->rating << endl;
                i++;
            }
            temp_show=temp_show->showNext;
        }
        current_node= current_node->next; 
    }
    return 0;
}
